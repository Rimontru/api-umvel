import { Router } from "express";
const router = Router();

import {seedUsers} from '../database/seeds/user.seed';
import {seedItems} from '../database/seeds/item.seed';

/**
 * @swagger
 * /api/seed/users:
 *  get:
 *    summary: Use to fill table users
 *    tags: [Seeders]
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.get('/seed/users', seedUsers);

/**
 * @swagger
 * /api/seed/items:
 *  get:
 *    summary: Use to fill table items
 *    tags: [Seeders]
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.get('/seed/items', seedItems);

export default router;