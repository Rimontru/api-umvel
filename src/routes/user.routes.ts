import { Router } from "express";
import { TokenValidation } from '../libs/verifyToken.lib'
const router = Router();

import {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
} from "../controllers/user.controller";


/**
 * @swagger
 * /api/users/{skip}/{take}:
 *  get:
 *    summary: Use to request paginate users
 *    security:
 *      - bearerAuth: []
 *    tags: [Users]
 *    parameters:
 *      - in: path
 *        name: skip
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 0
 *      - in: path
 *        name: take
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 10
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.get("/users/:skip/:take", TokenValidation, getUsers);

/**
 * @swagger
 * /api/users/{id}:
 *  get:
 *    summary: Use to find by user id
 *    security:
 *      - bearerAuth: []
 *    tags: [Users]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 1
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.get("/users/:id", TokenValidation, getUser);

/**
 * swagger
 * /api/users:
 *  post:
 *    summary: Use to create user
 *    security:
 *      - bearerAuth: []
 *    tags: [Users]
 *    requestBody:
 *      required: true
 *      content: 
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *            required:
 *              - name
 *            example: 
 *              name: richi
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 
router.post("/users", createUser);
*/

/**
 * @swagger
 * /api/users/{id}:
 *  put:
 *    summary: Use to update information user
 *    security:
 *      - bearerAuth: []
 *    tags: [Users]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 1
 *    requestBody:
 *      required: true
 *      content: 
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *            required:
 *              - name
 *            example: 
 *              name: richi
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.put("/users/:id", TokenValidation, updateUser);


/**
 * @swagger
 * /api/users/{id}:
 *  delete:
 *    summary: Use to delete recrod by user id
 *    security:
 *      - bearerAuth: []
 *    tags: [Users]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 1
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.delete("/users/:id", TokenValidation, deleteUser);

export default router;
