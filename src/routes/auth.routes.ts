import { Router } from "express";
const router = Router();

import { 
  signup,
  signin,
} from '../controllers/auth.controller';

/**
 * @swagger
 * /api/signup:
 *  post:
 *    summary: Use to signup user
 *    tags: [Auth]
 *    requestBody:
 *      required: true
 *      content: 
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *            required:
 *              - email
 *              - password
 *            example: 
 *              email: richi@test.com
 *              password: richito
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
*/ 
router.post("/signup", signup);

/**
 * @swagger
 * /api/signin:
 *  post:
 *    summary: Use to signin user
 *    tags: [Auth]
 *    requestBody:
 *      required: true
 *      content: 
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *            required:
 *              - email
 *              - password
 *            example: 
 *              email: richi@test.com
 *              password: richito
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
*/ 
router.post("/signin", signin);


export default router;
