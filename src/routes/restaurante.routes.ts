import { Router } from "express";
import { TokenValidation } from '../libs/verifyToken.lib'
const router = Router();

import {
  getItems,
  createOrder,
  updateStatusOrder,
  getOrderByStatus,
} from "../controllers/restaurante.controller";

/**
 * @swagger
 * /api/restaurant/items/{skip}/{take}:
 *  get:
 *    summary: Use to request paginate items
 *    security:
 *      - bearerAuth: []
 *    tags: [Restaurant]
 *    parameters:
 *      - in: path
 *        name: skip
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 0
 *      - in: path
 *        name: take
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 10
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.get("/restaurant/items/:skip/:take", TokenValidation, getItems);

/**
 * @swagger
 * /api/restaurant:
 *  post:
 *    summary: Use to create order
 *    security:
 *      - bearerAuth: []
 *    tags: [Restaurant]
 *    requestBody:
 *      required: true
 *      content: 
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              customer:
 *                type: string
 *              items:
 *                type: array
 *            required:
 *              - customer
 *              - items
 *            example: 
 *              customer: publico general
 *              items: [39,40]
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.post("/restaurant", TokenValidation, createOrder);

/**
 * @swagger
 * /api/restaurant/order/{id}:
 *  put:
 *    summary: Use to update status order
 *    security:
 *      - bearerAuth: []
 *    tags: [Restaurant]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema: 
 *          type: integer
 *        required: true
 *    requestBody:
 *      required: true
 *      content: 
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              statusOrder:
 *                type: string
 *                description: active/inactive/pending/delivered/finalized
 *            required:
 *              - statusOrder
 *            example: 
 *              statusOrder: inactive
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
router.put("/restaurant/order/:id", TokenValidation, updateStatusOrder);

/**
 * @swagger
 * /api/restaurant/order/{statusOrder}/{skip}/{take}:
 *  get:
 *    summary: Use to request paginate orders by status
 *    security:
 *      - bearerAuth: []
 *    tags: [Restaurant]
 *    parameters:
 *      - in: path
 *        name: skip
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 0
 *      - in: path
 *        name: take
 *        schema: 
 *          type: integer
 *        required: true
 *        default: 10
 *      - in: path
 *        name: statusOrder
 *        schema: 
 *          type: string
 *        required: true
 *        default: pending
 *        description: active/inactive/pending/delivered/finalized
 *    responses:
 *      200:
 *        description: success
 *      400:
 *        description: error
 */
 router.get("/restaurant/order/:statusOrder/:skip/:take", TokenValidation, getOrderByStatus);

export default router;