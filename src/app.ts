import "reflect-metadata";

import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors'
import dotenv from 'dotenv';
import { createConnections } from 'typeorm';
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

// Define routes
import authRoutes from './routes/auth.routes';
import userRoutes from './routes/user.routes';
import restauranteRoutes from './routes/restaurante.routes';
import seedRoutes from './routes/seeds.routes';

// Environment setting
dotenv.config();

// Definitions
const app: Application = express();
const port = process.env.PORT;

// swagger
const swaggerSpec = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "UMVEL API Information",
      version: "1.0.0"
    },
    servers: [
      {
        url: `http://localhost:${port}`,
      }
    ],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        }
      }
    },
    security: [{
      bearerAuth: []
    }]
  },
  apis: [`dist/routes/**/*.js`]
};

// up swagger
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerJsDoc(swaggerSpec)));

// Database connections
createConnections();

// settings
app.set('port', 3000 || port);

// Middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

// Routes
app.use('/api', [authRoutes, userRoutes, restauranteRoutes, seedRoutes]);

// Listening
app.listen(port);
console.log(`API listening at http://localhost:${port}`);


export default app;