import { Request, Response } from "express";
import { getConnection } from "typeorm";
import { Item } from "../models/restaurante/Item";

// Seed to Users
export const seedItems = async (req: Request, res: Response): Promise<Response> => {
  try {

    // Clear Table
    await getConnection("restaurante")
    .createQueryBuilder()
    .delete()
    .from(Item)
    .where("id >= :id", { id: 1 })
    .execute();

    // Insert Records
    await getConnection("restaurante")
    .createQueryBuilder()
    .insert()
    .into(Item)
    .values([
        { name: "Filete de pescado", price: 350}, 
        { name: "Milanesa de pollo", price: 420}, 
        { name: "Corte de brisket", price: 550}, 
        { name: "Sopa de mariscos", price: 250}, 
        { name: "Pozole mexicano rojo", price: 650}, 
     ])
    .execute();

    return res.status(200).json({status:200, message: 'Seed Items Successfully'});

  } catch (e) {
    return res.status(400).send({status:400, message: 'Error Seed Items'});
  }
  
};