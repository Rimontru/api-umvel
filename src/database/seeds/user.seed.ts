import { Request, Response } from "express";
import { getConnection, getRepository } from "typeorm";
import faker from "faker";
import bcrypt from 'bcryptjs';
import { validate } from "class-validator";
import { User } from "../models/usuarios/User";

// Seed to Users
export const seedUsers = async (req: Request, res: Response): Promise<Response> => {
  try {

    // Clear Table
    await getConnection("usuarios")
    .createQueryBuilder()
    .delete()
    .from(User)
    .where("id >= :id", { id: 1 })
    .execute();

    for (let i=0; i<=5; i++) {

      let user = new User();

      const salt = await bcrypt.genSalt(10);
      const pwd_bcrypt = await bcrypt.hash(faker.lorem.word(), salt);

      user.name = faker.name.findName();
      user.username = faker.name.firstName().toLowerCase();
      user.email = faker.internet.email();
      user.password = pwd_bcrypt;

      const errors = await validate(user);
      
      if (errors.length > 0)
        throw new Error(`Validation failed => ${errors}`); 
      
      await getRepository(User, "usuarios").save(user);
    }

    return res.status(200).json({status:200, message: 'Seed Users Successfully'});

  } catch (e) {
    return res.status(400).send({status:400, message: 'Error Seed Users'});
  }
  
};