import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Length } from "class-validator";

@Entity({ database: "usuarios" })

export class User extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  @Length(1, 255)
  name: string

  @Column({ nullable: true })
  @Length(1, 255)
  username: string

  @Column({ nullable: true })
  @Length(1, 255)
  email: string

  @Column({ nullable: true })
  @Length(1, 255)
  password: string
  
}