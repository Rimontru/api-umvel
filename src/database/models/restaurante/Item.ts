import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { IsInt, Length } from "class-validator";

@Entity({ database: "restaurante" })
export class Item extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  @Length(1, 255)
  name: string

  @Column()
  @IsInt()
  price: number

}