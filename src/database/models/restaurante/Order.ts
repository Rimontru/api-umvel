import { BaseEntity, Column, JoinColumn, OneToMany, Entity, PrimaryGeneratedColumn } from "typeorm";
import { IsInt, Length } from "class-validator";
import { OrderItem } from "./OrderItem";

@Entity({ database: "restaurante" })

export class Order extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @OneToMany(type => OrderItem, order_item => order_item.item) 
  @JoinColumn()
  order_items: OrderItem

  @Column()
  @IsInt()
  subtotal: number

  @Column()
  @IsInt()
  vat: number

  @Column()
  @IsInt()
  total: number

  @Column()
  @Length(1, 255)
  token: string

  @Column()
  @IsInt()
  total_items: number

  @Column()
  @Length(1, 255)
  customer_name: string

  @Column()
  @Length(1, 255)
  status: string

}