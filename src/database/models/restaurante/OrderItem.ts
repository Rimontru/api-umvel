import { BaseEntity, Entity, PrimaryGeneratedColumn, JoinColumn, OneToOne, ManyToOne } from "typeorm";
import {Order} from "./Order";
import {Item} from "./Item";

@Entity({ database: "restaurante" })

export class OrderItem extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne(type => Order, order => order.id) 
  @JoinColumn()
  order: Order

  @OneToOne(type => Item)
  @JoinColumn()
  item: Item
}