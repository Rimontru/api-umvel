import { Request, Response } from "express";
import { getConnection, getRepository } from "typeorm";
import { User } from "../database/models/usuarios/User";
import { validate } from "class-validator";


// Response JSON
let jsonResponse = {
  status: 200,
  message: "Success",
  data: {}
}


export const getUsers = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {skip, take} = req.params;
    const _skip = skip ? parseInt(skip) : 0;
    const _take = take ? parseInt(take) : 10;

    // get Users
    const result = await getConnection("usuarios")
    .createQueryBuilder()
    .select("user")
    .from(User,"user")
    .skip(_skip)
    .take(_take)
    .getMany();

    jsonResponse.data = result;

    return res.status(200).json(jsonResponse);

  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};

export const getUser = async (req: Request, res: Response): Promise<Response> => {
  try{
    const {id} = req.params;

    // get User
    const result = await getRepository(User, "usuarios").findOne(id) || {};
    
    jsonResponse.data = result;

    return res.status(200).json(jsonResponse);

  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};

export const createUser = async (req: Request, res: Response): Promise<Response> => {
  try{
    const {name} = req.body;

    let user = new User();
    user.name = name;

    const errors = await validate(user);
    
    if (errors.length > 0) {
      throw new Error(`Validation failed => ${errors}`); 
    } else {

      const result = await getRepository(User, "usuarios").save(user);

      jsonResponse.data = result;

      return res.status(200).json(jsonResponse);
    }

  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};

export const updateUser = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {id} = req.params;
    const {name} = req.body;

    let user = new User();
    user.name = name;
    user.username = name;
    user.password = name;
    user.email = name;

    const errors = await validate(user);
    if (errors.length > 0)
      throw new Error(`Validation failed => ${errors}`); 

    const findUser = await getRepository(User, "usuarios").findOne(id);

    if (!findUser)
      throw new Error(`User not find!`);
    
    // merge new data
    getRepository(User, "usuarios").merge(findUser, {name});

    // save to update user
    const result = await getRepository(User, "usuarios").save(findUser);

    jsonResponse.data = result;

    return res.status(200).json(jsonResponse);
    
  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};

export const deleteUser = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {id} = req.params;

    const result = await getRepository(User, "usuarios").delete(id) || {};

    jsonResponse.data = result;

    return res.status(200).json(jsonResponse);
    
  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};