import { Request, Response } from "express";
import { getConnection, getManager } from "typeorm";
import { Item } from "../database/models/restaurante/Item";
import { Order } from "../database/models/restaurante/Order";
import { OrderItem } from "../database/models/restaurante/OrderItem";
import { v4 as uuidv4 } from 'uuid';
import { validate } from "class-validator";

// Response JSON
let jsonResponse = {
  status: 200,
  message: "Success",
  data: {}
}

// get Items
export const getItems = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {skip, take} = req.params;
    const _skip = skip ? parseInt(skip) : 0;
    const _take = take ? parseInt(take) : 10;

    // Fetch Records Items
    const result = await getConnection("restaurante")
    .createQueryBuilder()
    .select("item")
    .from(Item,"item")
    .skip(_skip)
    .take(_take)
    .getMany();

    jsonResponse.data = result;

    return res.status(200).json(jsonResponse);

  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
  
};

// create Order
export const createOrder = async (req: Request, res: Response): Promise<Response> => {
  try {
    // 
    const {items, customer} = req.body;
    const uuid = uuidv4();
    const iva = 0.25;
    let total = 0, subtotal = 0, vat = 0;

    // Static Connection
    const connection = getConnection("restaurante");
    
    // Get Items
    const itemsOrder = await getConnection("restaurante")
    .createQueryBuilder()
    .select("item")
    .from(Item,"item")
    .where("item.id IN (:ids)", { ids: items })
    .getMany();

    // Operations
    for (let i in itemsOrder) {
      const item = itemsOrder[i];
      subtotal += item.price; 
      vat += (item.price * iva);
    }

    total = (subtotal + vat);

    // Object to Save
    let order = new Order();
    order.subtotal = subtotal;
    order.vat = vat;
    order.total = total;
    order.token = uuid;
    order.total_items = itemsOrder.length;
    order.customer_name = customer;
    order.status = "active";

    // Validation Model
    const errors = await validate(order);
    
    if (errors.length > 0) {
      throw new Error(`Validation failed => ${errors}`); 

    } else {

      // Insert Record Order
      const orderSave = await connection.manager.save(order);
      let item = new Item();
      let order2 = new Order();

      for (let i in itemsOrder) {
        const sitem = itemsOrder[i];
        order2.id = orderSave.id
        item.id = sitem.id
        
        // Insert Relation Order - Item
        await getConnection("restaurante")
        .createQueryBuilder()
        .insert()
        .into(OrderItem)
        .values({
          order: order2,
          item: item
        })
        .execute();
      }

      jsonResponse.data = orderSave;
    }

    return res.status(200).json(jsonResponse);

  } catch (e: unknown) {
    jsonResponse.status = 400;

    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }
    
    return res.status(400).send({jsonResponse});
  }
};

// update Order
export const updateStatusOrder = async (req: Request, res: Response): Promise<Response> => {
  try {
    // 
    const { statusOrder } = req.body;
    const { id } = req.params;
    
    // Get Items
    const updOrder = await getConnection("restaurante")
    .createQueryBuilder()
    .update(Order)
    .set({ status: statusOrder })
    .where("id = :id", { id: id })
    .execute();

    jsonResponse.data = updOrder;

    return res.status(200).json(jsonResponse);

  } catch (e: unknown) {
    jsonResponse.status = 400;

    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }
    
    return res.status(400).send({jsonResponse});
  }
};

// get Order by status
export const getOrderByStatus = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {skip, take, statusOrder} = req.params;
    const _skip = skip ? parseInt(skip) : 0;
    const _take = take ? parseInt(take) : 10;

    // Fetch Records Items
    const result = await getConnection("restaurante")
    .createQueryBuilder()
    .select("order")
    .from(Order,"order")
    .where("status = :status", { status: statusOrder })
    .skip(_skip)
    .take(_take)
    .getMany();

    jsonResponse.data = result;

    return res.status(200).json(jsonResponse);

  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
  
};