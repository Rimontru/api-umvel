import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import faker from "faker";
import { User } from "../database/models/usuarios/User";


// Response JSON
let jsonResponse = {
  status: 200,
  message: "Success",
  data: {}
}

//
export const signup = async (req: Request, res: Response): Promise<Response> => {
  try{
    const {email, password} = req.body;

    let user = new User();

    const salt = await bcrypt.genSalt(10);
    const pwd_bcrypt = await bcrypt.hash(password, salt);

    user.name = faker.name.findName();
    user.username = faker.name.firstName().toLowerCase();
    user.email = email;
    user.password = pwd_bcrypt;

    const errors = await validate(user);
    
    if (errors.length > 0)
      throw new Error(`Validation failed => ${errors}`);

    const savedUser = await getRepository(User, "usuarios").save(user);

    const token: string = jwt.sign({ username: savedUser.username }, process.env['TOKEN_SECRET'] || '', {
      expiresIn: 60 * 60 * 24
    });

    res.header('bearerAuth', token).json(savedUser);

    jsonResponse.data = savedUser;

    return res.status(200).json(jsonResponse);


  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};

//
export const signin = async (req: Request, res: Response): Promise<Response> => {
  try{
    const {email, password} = req.body;
   
    const findUser = await getRepository(User, "usuarios").findOne({email: email});

    if (!findUser)
      throw new Error(`Email not found!`);
    
    const password_valid = await bcrypt.compare(password, findUser.password);

    if(!password_valid)
      throw new Error(`Password invalid!`);

    const token: string = jwt.sign({ username: findUser.username }, process.env['TOKEN_SECRET'] || '', {
      expiresIn: 60 * 60 * 24
    });

    res.header('bearerAuth', token).json(findUser);

    jsonResponse.data = findUser;

    return res.status(200).json(jsonResponse);


  } catch (e: unknown) {
    jsonResponse.status = 400;
    
    if (typeof e === "string") {
      jsonResponse.message = `Error ${e.toUpperCase()}`;
    } else if (e instanceof Error) {
      jsonResponse.message = `Error ${e.message}`;
    }

    return res.status(400).send(jsonResponse);
  }
};