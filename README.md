# Api Umvel

Prueba técnica

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:de4ed2ced2ebfa17855357d90dae61df?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:de4ed2ced2ebfa17855357d90dae61df?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:de4ed2ced2ebfa17855357d90dae61df?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Rimontru/api-umvel.git
git branch -M main
git push -uf origin main
```

## Libraries dependencies

- [ ] [express](https://github.com/expressjs/express) for api web server.
- [ ] [cors](https://github.com/expressjs/cors) for middleware enable CORS.
- [ ] [morgan](https://github.com/expressjs/morgan) HTTP request logger middleware.
- [ ] [dotenv](https://github.com/motdotla/dotenv) for environment variables.
- [ ] [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) for json web token app.
- [ ] [typeorm](https://github.com/typeorm/typeorm) for ORM.
- [ ] [mysql](https://github.com/mysqljs/mysql) for mysql.
- [ ] [class-validator](https://github.com/typestack/class-validator) for validation model.
- [ ] [uuid](https://github.com/uuidjs/uuid) for creation of RFC4122 UUIDs.
- [ ] [swagger-jsdoc](https://github.com/uuidjs/uuid) for API documentation.
- [ ] [swagger-ui-express](https://github.com/uuidjs/uuid) for swagger Interface.
- [ ] [bcryptjs](https://github.com/dcodeIO/bcrypt.js) for bcrypt strings.

***

## Libraries devDependencies

- [ ] [typescript](https://github.com/Microsoft/TypeScript) for types developer app.
- [ ] [tsc-watch](https://github.com/gilamran/tsc-watch) The nodemon for TypeScript.
- [ ] [ts-node](https://github.com/TypeStrong/ts-node) TypeScript execution.
- [ ] [@types] for code recoginer packages to TypeScript.

***

## Environments

- [ ] [npm-run-watch] command for developer app
- [ ] [npm-run-deploy] command for deployment app
- [ ] [npm-run-build] command for building app

***

## Documentation API

- [ ] [swagger](http://localhost:3000/api-docs) swagger api docs.

***